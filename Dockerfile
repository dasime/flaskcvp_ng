FROM python:3.9-slim AS base

WORKDIR /srv


FROM base AS build

ENV PIP_DISABLE_PIP_VERSION_CHECK=1
ENV PIP_NO_CACHE_DIR=1

RUN pip install poetry
RUN python -m venv /venv
RUN apt-get update && \
    apt-get install -y build-essential libssl-dev libbz2-dev

COPY . /srv

RUN poetry build
RUN /venv/bin/pip install dist/*.whl


FROM base AS deploy

ENV PATH="/venv/bin:$PATH"
ENV FLASKCVP_NG_HOST="0.0.0.0"
ENV MURMUR_SLICEFILE="/srv/Murmur.ice"

EXPOSE 5000

COPY --from=build /venv /venv
COPY --from=build /srv/Murmur.ice /srv

ENTRYPOINT ["flaskcvp_ng"]
