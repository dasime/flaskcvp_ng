# Contributing

Please ensure new code passes unit tests, linting, and the integrations at
least superficially appear to work.

## Unit Tests

There are a handful of unit tests that survived from `mumble-django`.

```sh
poetry run task test
```

## Linting

Code is expected to confirm to [Black](https://github.com/psf/black) and
[isort](https://github.com/PyCQA/isort).

```sh
poetry run task lint
```

## Integration Tests

There are some very simple integration tests available using `docker-compose`
to confirm flaskcvp_ng works with some popular Mumble containers.

They can be run with a command such as:

```sh
docker-compose -f integration/docker-compose_phlak.yaml up
```

Other than confirming the connection with the `docker-compose` logs,
flaskcvp_ng can also be connected to with `curl` or with your web browser:

```sh
curl localhost:5000/1
{
  "id": 1,
  "name": "",
  "root": {
    "channels": [],
    "description": "",
    "id": 0,
    "links": [],
    "name": "Root",
    "parent": -1,
    "position": 0,
    "temporary": false,
    "users": []
  },
  "x_connecturl": "mumble://mumble.example.com:64738"
}%
```

## Local Python Build Instructions

Either run it from source:

```sh
poetry install
poetry run flaskcvp_ng -s "./Murmur.ice"
```

Or build a wheel and install that:

```sh
poetry build
pip install dist/*.whl
flaskcvp_ng -s "./Murmur.ice"
```

### Publishing

Build and push the artefacts to GitLab:

```sh
poetry publish \
  --build \
  -r flaskcvpng \
  -u __token__ \
  -p $GITLAB_TOKEN_HERE
```

## Local Docker Build Instructions

Authenticate to GitLab:

```sh
docker login "registry.gitlab.com" -u <username> -p <token>
```

Build the image:

```sh
docker build -t "registry.gitlab.com/dasime/flaskcvp_ng:$(poetry version --short)" .
```

Push the image:

```sh
docker push "registry.gitlab.com/dasime/flaskcvp_ng:$(poetry version --short)"
```

For more on the GitLab container registry see the docs here:
<https://docs.gitlab.com/ee/user/packages/container_registry/index.html>
