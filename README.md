# flaskcvp_ng

Source: <https://gitlab.com/dasime/flaskcvp_ng>

A minimal [Mumble](https://www.mumble.info/)
[channel viewer protocol](https://wiki.mumble.info/wiki/Channel_Viewer_Protocol)
server.

In short. Connect this to a running Mumble VOIP server to generate a JSON
feed of channels and connected users.

This feed can be consumed by an
[existing Channel Viewer](https://wiki.mumble.info/wiki/3rd_Party_Applications#Channel_Viewers)
or is simple enough you could write your own.

## Running

### Docker

The easiest way to run flaskcvp_ng is using a docker container:

```sh
docker run \
    -p 5000:5000 \
    -e MURMUR_ICE_HOST=mumble \
    -e MURMUR_CONNECT_URL=mumble://localhost:64738 \
    --link mumble:mumble \
    registry.gitlab.com/dasime/flaskcvp_ng:0.3.1
```

### Python

If docker isn't your thing, you can still run the project directly in Python.

The minimum Python version is: `3.9`

```sh
pip install flaskcvp-ng --extra-index-url https://gitlab.com/api/v4/projects/24267675/packages/pypi/simple
flaskcvp_ng -s "./Murmur.ice"
```

NOTE: It might take a couple of minutes to install `zeroc-ice` as wheels are not available.

For instructions on building or running from source, see `./CONTRIBUTING.md`.

### Docker Compose

There are some examples for using flaskcvp_ng with Mumble using `docker-compose`
in the `./integrations` directory.

## Usage

Server list is available at `http://HOST:5000/`.

Server channels are viewable at `http://HOST:5000/SERVER_ID`.

### Configuration

flaskcvp_ng can be configured either using command line arguments or
environment variables:

| Command Line Argument                      | Environment Variable  | Description
| ---                                        | ---                   | ---
| `-c CONNSTRING`, `--connstring CONNSTRING` | _None_                | Manually set the complete connection string. For most usecases this will not be necessary as most users only need to set the host and port. In previous versions this was the only way to configure flaskcvp. Now it is instead recommend to use the below `--ice-*` options.
| `--ice-encoding ICE_ENCODING`              | `MURMUR_ICE_ENCODING` | Which ICE encoding to use. Default is `1.0`.
| `--ice-host ICE_HOST`                      | `MURMUR_ICE_HOST`     | Address of Mumble server to monitor. Default is `127.0.0.1`.
| `--ice-port ICE_PORT`                      | `MURMUR_ICE_PORT`     | Port of Mumble server to monitor. Default is `6502`.
| `--connect-url CONNECT_URL`                | `MURMUR_CONNECT_URL`  | The connection string of the server.
| `-i ICESECRET`, `--icesecret ICESECRET`    | _None_                | Ice secret to use in the connection. Also see `--asksecret`.
| `-a`, `--asksecret`                        | _None_                | Ask for the Ice secret on the shell instead of taking it from the command line.
| `-s SLICE`, `--slice SLICE`                | `MURMUR_SLICEFILE`    | Path to the slice file. Default is `/usr/share/slice/Murmur.ice`.
| `-d`, `--debug`                            | _None_                | Enable error debugging
| `-H HOST`, `--host HOST`                   | `FLASKCVP_NG_HOST`    | The IP to bind to. Default is `127.0.0.1`.
| `-p PORT`, `--port PORT`                   | `FLASKCVP_NG_PORT`    | The port number to bind to. Default is `5000`.

## Thanks

With thanks to [Michael Ziegler](https://svedr.in/) for their original work on
[mumble-django](https://gitlab.com/dasime/mumble-django) and to
[fallendusk](https://github.com/fallendusk/) for their initial work on
containerising flaskcvp.
