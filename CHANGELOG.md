# CHANGELOG

## 0.3.1 / 2022-03-02

* Publish Python packages in addition to Docker images
* Update dependencies
* Minor formatting for latest linter releases
* Add support for Mumble 1.4 series
* Update integration test/example for the latest version of the official image
* Refreshed base Python `3.9-slim` image

## 0.3.0 / 2021-03-29

* Rename `README` -> `README.md`.
* Add `pyproject.toml` with `black` and `isort` code quality tooling.
  - Remove `setup.py`.
* Enforce `black` and `isort` standards.
* Convert from Python 2 to Python 3.
* Migrate bin script from `optparse` to `argparse`.
* Remove texture operations as they're not required for CVP.
  Drops requirement for deprecated `PIL` library.
* Correct import paths for the compiled version of `flaskcvp_ng`.
* Manage `flask` and `zeroc-ice` dependences with Poetry.
* Add new multi-stage `Dockerfile`:
  - Based on the latest Python 3.9.
  - Builds zeroc-ice from source.
  - Uses a smaller base image for a smaller image (from 256 MB to 140 MB).
  - Remove the bootstrap script.
      + Add additional command line arguments so users no longer need to
        build valid ICE connection strings.
      + Improve support for environment variables for passing in configuration.
* Modernise unit tests
* Remove or update definitely dead code.

## 0.2.1 / 2021-02-08

* Hardcode the Ice encoding version as 1.0 in the bootstrap connection string.
  This fixes connections for where `flaskcvp` using an older version of Ice
  can't communicate with Mumble using newer versions of Ice and vice-versa.
* Add simple integration test using `docker-compose`.

## 0.2.0 / 2021-02-07

Based on:
<https://github.com/fallendusk/murmur-cvp> `800ac4f1043324bb62f57b71047b1177e6016b87`

* Correct `flaskcvp_ng` source code path in the `Dockerfile` to account
  for path rewriting.

## 0.1.0

Based on:
<https://gitlab.com/dasime/mumble-django> `de51079acdef93559ec8636f193555cfa9ddb75f`
