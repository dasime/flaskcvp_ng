#!/usr/bin/env python
"""
 *  Copyright (C) 2010, Michael "Svedrin" Ziegler <diese-addy@funzt-halt.net>
 *
 *  Mumble-Django is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This package is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
"""
import getpass
import os
from argparse import ArgumentParser

from flask import Flask, jsonify

from flaskcvp_ng.mumble.mctl import MumbleCtlBase

DEFAULT_ICE_ENCODING = "1.0"
DEFAULT_ICE_HOST = "127.0.0.1"
DEFAULT_ICE_PORT = "6502"
DEFAULT_SLICEFILE = "/usr/share/slice/Murmur.ice"
DEFAULT_ICESECRET = None


def main():
    parser = ArgumentParser(
        description="""
This is a minimalistic implementation of a Channel Viewer Protocol provider
using the Flask Python framework and Mumble-Django's MCTL connection library.
"""
    )

    parser.add_argument(
        "-c",
        "--connstring",
        help="""Manually set the complete connection string.
For most usecases this will not be necessary as most users only need to set
the host and port.""",
        default=None,
    )

    parser.add_argument(
        "--ice-encoding",
        help=f"""
Which ICE encoding to use.
Default is '{DEFAULT_ICE_ENCODING}'.
Env var is 'MURMUR_ICE_ENCODING'.""",
        default=os.environ.get("MURMUR_ICE_ENCODING", DEFAULT_ICE_ENCODING),
    )

    parser.add_argument(
        "--ice-host",
        help=f"""
Address of Mumble server to monitor.
Default is '{DEFAULT_ICE_HOST}'.
Env var is 'MURMUR_ICE_HOST'.""",
        default=os.environ.get("MURMUR_ICE_HOST", DEFAULT_ICE_HOST),
    )

    parser.add_argument(
        "--ice-port",
        help=f"""
Port of Mumble server to monitor.
Default is '{DEFAULT_ICE_PORT}'.
Env var is 'MURMUR_ICE_PORT'.""",
        default=os.environ.get("MURMUR_ICE_PORT", DEFAULT_ICE_PORT),
    )

    parser.add_argument(
        "--connect-url",
        help="""
The connection string of the server.
Env var is 'MURMUR_CONNECT_URL'.""",
        default=os.environ.get("MURMUR_CONNECT_URL"),
    )

    parser.add_argument(
        "-i",
        "--icesecret",
        help="Ice secret to use in the connection. Also see --asksecret.",
        default=DEFAULT_ICESECRET,
    )

    parser.add_argument(
        "-a",
        "--asksecret",
        help="Ask for the Ice secret on the shell instead of taking it from the command line.",
        action="store_true",
        default=False,
    )

    parser.add_argument(
        "-s",
        "--slice",
        help=f"""
Path to the slice file.
Default is '{DEFAULT_SLICEFILE}'.
Env var is 'MURMUR_SLICEFILE'.""",
        default=os.environ.get("MURMUR_SLICEFILE", DEFAULT_SLICEFILE),
    )

    parser.add_argument(
        "-d",
        "--debug",
        help="Enable error debugging",
        default=False,
        action="store_true",
    )

    parser.add_argument(
        "-H",
        "--host",
        help="""
The IP to bind to.
Default is '127.0.0.1'.
Env var is 'FLASKCVP_NG_HOST'.""",
        default=os.environ.get("FLASKCVP_NG_HOST", "127.0.0.1"),
    )

    parser.add_argument(
        "-p",
        "--port",
        type=int,
        help="""
The port number to bind to.
Default is '5000'.
Env var is 'FLASKCVP_NG_PORT'.""",
        default=os.environ.get("FLASKCVP_NG_PORT", 5000),
    )

    options = parser.parse_args()

    # If you used `-c` before, it trumps new behaviour
    if options.connstring is None:
        options.connstring = f"Meta -e {options.ice_encoding}:tcp -h {options.ice_host} -p {options.ice_port}"

    if options.asksecret:
        options.icesecret = getpass.getpass("Ice secret: ")

    serve(options)


def serve(options):
    ctl = MumbleCtlBase.newInstance(
        options.connstring, options.slice, options.icesecret
    )

    app = Flask(__name__)

    def getUser(user):
        fields = [
            "channel",
            "deaf",
            "mute",
            "name",
            "selfDeaf",
            "selfMute",
            "session",
            "suppress",
            "userid",
            "idlesecs",
            "recording",
            "comment",
            "prioritySpeaker",
        ]
        return dict(
            list(zip(fields, [getattr(user, field) for field in fields]))
        )

    def getChannel(channel):
        fields = [
            "id",
            "name",
            "parent",
            "links",
            "description",
            "temporary",
            "position",
        ]
        data = dict(
            list(zip(fields, [getattr(channel.c, field) for field in fields]))
        )
        data["channels"] = [
            getChannel(subchan) for subchan in channel.children
        ]
        data["users"] = [getUser(user) for user in channel.users]
        return data

    @app.route("/<int:srv_id>")
    def getTree(srv_id):
        name = ctl.getConf(srv_id, "registername")
        tree = ctl.getTree(srv_id)

        serv = {
            "x_connecturl": options.connect_url,
            "id": srv_id,
            "name": name,
            "root": getChannel(tree),
        }

        return jsonify(serv)

    @app.route("/")
    def getServers():
        return jsonify(servers=ctl.getBootedServers())

    app.run(host=options.host, port=options.port, debug=options.debug)


if __name__ == "__main__":
    main()
