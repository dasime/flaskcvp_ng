"""
 *  Copyright © 2009-2014, Michael "Svedrin" Ziegler <diese-addy@funzt-halt.net>
 *
 *  Mumble-Django is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This package is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
"""

import sys
from unittest import mock

from flaskcvp_ng.mumble.MumbleCtlIce import MumbleCtlIce, loadSlice

# This is a magic module
sys.modules["Murmur"] = mock.MagicMock()


@mock.patch("flaskcvp_ng.mumble.MumbleCtlIce.Ice")
def test_loadSlice_getSliceDir(mock_Ice):
    mock_Ice.getSliceDir.return_value = "/tmp/slice"

    loadSlice("someslice")

    mock_Ice.loadSlice.assert_called()
    mock_Ice.loadSlice.assert_called_once()
    mock_Ice.loadSlice.assert_called_with("", ["-I/tmp/slice", "someslice"])


@mock.patch("flaskcvp_ng.mumble.MumbleCtlIce.Ice")
def test_loadSlice_getSliceDir_empty(mock_Ice):
    mock_Ice.getSliceDir.return_value = ""

    loadSlice("someslice")

    mock_Ice.loadSlice.assert_called()
    mock_Ice.loadSlice.assert_called_once()
    mock_Ice.loadSlice.assert_called_with("someslice")


@mock.patch("Murmur.MetaPrx")
@mock.patch("flaskcvp_ng.mumble.MumbleCtlIce.IcePy")
@mock.patch("flaskcvp_ng.mumble.MumbleCtlIce.Ice")
def test_MumbleCtlIce(mock_Ice, mock_IcePy, mock_MetaPrx):
    mock_MetaPrx.checkedCast().getVersion.return_value = (
        1,
        2,
        3,
        "12oi3j1",
    )

    MumbleCtlIce("Meta:tcp -h 127.0.0.1 -p 6502")

    mock_IcePy.Operation.assert_not_called()
    mock_Ice.createProperties().setProperty.assert_called()
    mock_Ice.createProperties().setProperty.assert_has_calls(
        [
            mock.call("Ice.ImplicitContext", "Shared"),
            mock.call("Ice.MessageSizeMax", "65535"),
        ]
    )

    mock_prx = mock_Ice.initialize().stringToProxy()
    mock_prx.ice_ping.assert_called()
