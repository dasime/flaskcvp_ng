"""
 *  Copyright © 2009, withgod                   <withgod@sourceforge.net>
 *         2009-2010, Michael "Svedrin" Ziegler <diese-addy@funzt-halt.net>
 *
 *  Mumble-Django is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This package is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
"""

import tempfile
from os import name as os_name
from os import unlink
from os.path import exists, join
from time import time

import Ice
import IcePy

from flaskcvp_ng.mumble.mctl import MumbleCtlBase
from flaskcvp_ng.mumble.utils import ObjectInfo


def loadSlice(slicefile):
    """Load the slice file with the correct include dir set, if possible."""
    if hasattr(Ice, "getSliceDir"):
        icepath = Ice.getSliceDir()
    else:
        icepath = None

    # Ice returns '' in getSliceDir() sometimes. (I kid you not.)
    if not icepath:
        # last resort when getSliceDir fails, won't work for >=1.2.3
        Ice.loadSlice(slicefile)
    else:
        Ice.loadSlice("", ["-I" + icepath, slicefile])


def MumbleCtlIce(connstring, slicefile=None, icesecret=None):
    """
    Choose the correct Ice handler to use (1.1.8 or 1.2.x), and make sure the
    Murmur version matches the slice Version.

    Optional parameters are the path to the slice file and the Ice secret
    necessary to authenticate to Murmur.

    The path can be omitted only if running Murmur 1.2.3 or later, which
    exports a getSlice method to retrieve the Slice from.
    """

    prop = Ice.createProperties([])
    prop.setProperty("Ice.ImplicitContext", "Shared")
    prop.setProperty("Ice.MessageSizeMax", "65535")

    idd = Ice.InitializationData()
    idd.properties = prop

    # TODO, be kind and destroy connectors in all conditions. See:
    # https://doc.zeroc.com/ice/3.7/language-mappings/python-mapping/initialization-in-python
    ice = Ice.initialize(idd)

    if icesecret:
        ice.getImplicitContext().put("secret", icesecret)

    prx = ice.stringToProxy(connstring)

    try:
        prx.ice_ping()
    except Ice.Exception:
        raise OSError(
            "Murmur does not appear to be listening on this address (Ice ping failed)."
        )

    try:
        import Murmur
    except ImportError:
        # Try loading the Slice from Murmur directly via its getSlice method.
        # See scripts/testdynamic.py in Mumble's Git repository.
        try:
            slice = IcePy.Operation(
                "getSlice",
                Ice.OperationMode.Idempotent,
                Ice.OperationMode.Idempotent,
                True,
                (),
                (),
                (),
                IcePy._t_string,
                (),
            ).invoke(prx, ((), None))
        except (TypeError, Ice.OperationNotExistException):
            if not slicefile:
                raise OSError(
                    "You didn't configure a slice file. Please set the SLICE variable in settings.py."
                )
            if not exists(slicefile):
                raise OSError(
                    "The slice file does not exist: '%s' - please check the settings."
                    % slicefile
                )
            if " " in slicefile:
                raise OSError(
                    "You have a space char in your Slice path. This will confuse Ice, please check."
                )
            if not slicefile.endswith(".ice"):
                raise OSError("The slice file name MUST end with '.ice'.")

            try:
                loadSlice(slicefile)
            except RuntimeError:
                raise RuntimeError(
                    "Slice preprocessing failed. Please check your server's error log."
                )
        else:
            if os_name == "nt":
                # It weren't Windows if it didn't need to be treated differently. *sigh*
                temppath = join(tempfile.gettempdir(), "Murmur.ice")
                slicetemp = open(temppath, "w+b")
                try:
                    slicetemp.write(slice)
                finally:
                    slicetemp.close()
                try:
                    loadSlice(temppath)
                except RuntimeError:
                    raise RuntimeError(
                        "Slice preprocessing failed. Please check your server's error log."
                    )
                finally:
                    unlink(temppath)
            else:
                slicetemp = tempfile.NamedTemporaryFile(suffix=".ice")
                try:
                    slicetemp.write(slice)
                    slicetemp.flush()
                    loadSlice(slicetemp.name)
                except RuntimeError:
                    raise RuntimeError(
                        "Slice preprocessing failed. Please check your server's error log."
                    )
                finally:
                    slicetemp.close()

        import Murmur

    meta = Murmur.MetaPrx.checkedCast(prx)

    murmurversion = meta.getVersion()[:3]

    if murmurversion == (1, 1, 8):
        return MumbleCtlIce_118(connstring, meta)

    elif murmurversion[:2] == (1, 2):
        if murmurversion[2] < 2:
            return MumbleCtlIce_120(connstring, meta)

        elif murmurversion[2] == 2:
            return MumbleCtlIce_122(connstring, meta)

        elif murmurversion[2] >= 3:
            return MumbleCtlIce_123(connstring, meta)

    elif murmurversion[:2] == (1, 3) or murmurversion[:2] == (1, 4):
        return MumbleCtlIce_123(connstring, meta)

    raise NotImplementedError(
        "No ctl object available for Murmur version %d.%d.%d"
        % tuple(murmurversion)
    )


class MumbleCtlIce_118(MumbleCtlBase):
    method = "ICE"

    def __init__(self, connstring, meta):
        self.proxy = connstring
        self.meta = meta

    def _getIceServerObject(self, srvid):
        return self.meta.getServer(srvid)

    def getBootedServers(self):
        ret = []
        for x in self.meta.getBootedServers():
            ret.append(x.id())
        return ret

    def getVersion(self):
        return self.meta.getVersion()

    def getAllServers(self):
        ret = []
        for x in self.meta.getAllServers():
            ret.append(x.id())
        return ret

    def getRegisteredPlayers(self, srvid, filter=""):
        users = self._getIceServerObject(srvid).getRegisteredPlayers(filter)
        ret = {}

        for user in users:
            ret[user.playerid] = ObjectInfo(
                userid=int(user.playerid),
                name=str(user.name, "utf8"),
                email=str(user.email, "utf8"),
                pw=str(user.pw, "utf8"),
            )

        return ret

    def getChannels(self, srvid):
        return self._getIceServerObject(srvid).getChannels()

    def getTree(self, srvid):
        return self._getIceServerObject(srvid).getTree()

    def getPlayers(self, srvid):
        users = self._getIceServerObject(srvid).getPlayers()

        ret = {}

        for useridx in users:
            user = users[useridx]
            ret[user.session] = ObjectInfo(
                session=user.session,
                userid=user.playerid,
                mute=user.mute,
                deaf=user.deaf,
                suppress=user.suppressed,
                selfMute=user.selfMute,
                selfDeaf=user.selfDeaf,
                channel=user.channel,
                name=user.name,
                onlinesecs=user.onlinesecs,
                bytespersec=user.bytespersec,
            )

        return ret

    def getDefaultConf(self):
        return self.setUnicodeFlag(self.meta.getDefaultConf())

    def getAllConf(self, srvid):
        conf = self.setUnicodeFlag(
            self._getIceServerObject(srvid).getAllConf()
        )

        info = {}
        for key in conf:
            if key == "playername":
                info["username"] = conf[key]
            else:
                info[str(key)] = conf[key]
        return info

    def newServer(self):
        return self.meta.newServer().id()

    def isBooted(self, srvid):
        return bool(self._getIceServerObject(srvid).isRunning())

    def start(self, srvid):
        self._getIceServerObject(srvid).start()

    def stop(self, srvid):
        self._getIceServerObject(srvid).stop()

    def deleteServer(self, srvid):
        if self._getIceServerObject(srvid).isRunning():
            self._getIceServerObject(srvid).stop()
        self._getIceServerObject(srvid).delete()

    def setSuperUserPassword(self, srvid, value):
        self._getIceServerObject(srvid).setSuperuserPassword(value)

    def getConf(self, srvid, key):
        if key == "username":
            key = "playername"

        return self._getIceServerObject(srvid).getConf(key)

    def setConf(self, srvid, key, value):
        if key == "username":
            key = "playername"
        if value is None:
            value = ""
        self._getIceServerObject(srvid).setConf(key, value)

    def registerPlayer(self, srvid, name, email, password):
        mumbleid = self._getIceServerObject(srvid).registerPlayer(name)
        self.setRegistration(srvid, mumbleid, name, email, password)
        return mumbleid

    def unregisterPlayer(self, srvid, mumbleid):
        self._getIceServerObject(srvid).unregisterPlayer(mumbleid)

    def getRegistration(self, srvid, mumbleid):
        user = self._getIceServerObject(srvid).getRegistration(mumbleid)
        return ObjectInfo(
            userid=mumbleid,
            name=user.name,
            email=user.email,
            pw="",
        )

    def setRegistration(self, srvid, mumbleid, name, email, password):
        import Murmur

        user = Murmur.RegisteredPlayer()
        user.playerid = mumbleid
        user.name = name
        user.email = email
        user.pw = password
        # update*r*egistration r is lowercase...
        return self._getIceServerObject(srvid).updateregistration(user)

    def getACL(self, srvid, channelid):
        # need to convert acls to say "userid" instead of "playerid". meh.
        raw_acls, raw_groups, raw_inherit = self._getIceServerObject(
            srvid
        ).getACL(channelid)

        acls = [
            ObjectInfo(
                applyHere=rule.applyHere,
                applySubs=rule.applySubs,
                inherited=rule.inherited,
                userid=rule.playerid,
                group=rule.group,
                allow=rule.allow,
                deny=rule.deny,
            )
            for rule in raw_acls
        ]

        return acls, raw_groups, raw_inherit

    def setACL(self, srvid, channelid, acls, groups, inherit):
        import Murmur

        ice_acls = []

        for rule in acls:
            ice_rule = Murmur.ACL()
            ice_rule.applyHere = rule.applyHere
            ice_rule.applySubs = rule.applySubs
            ice_rule.inherited = rule.inherited
            ice_rule.playerid = rule.userid
            ice_rule.group = rule.group
            ice_rule.allow = rule.allow
            ice_rule.deny = rule.deny
            ice_acls.append(ice_rule)

        return self._getIceServerObject(srvid).setACL(
            channelid, ice_acls, groups, inherit
        )

    def verifyPassword(self, srvid, username, password):
        return self._getIceServerObject(srvid).verifyPassword(
            username, password
        )

    @staticmethod
    def setUnicodeFlag(data):
        ret = ""
        if (
            isinstance(data, tuple)
            or isinstance(data, list)
            or isinstance(data, dict)
        ):
            ret = {}
            for key in list(data.keys()):
                ret[
                    MumbleCtlIce_118.setUnicodeFlag(key)
                ] = MumbleCtlIce_118.setUnicodeFlag(data[key])
        else:
            ret = str(data, "utf-8")

        return ret

    def getUptime(self, srvid):
        return None

    def getBans(self, srvid):
        return self._getIceServerObject(srvid).getBans()

    def getLog(self, srvid, first=0, last=100):
        return self._getIceServerObject(srvid).getLog(first, last)

    def addChannel(self, srvid, name, parentid):
        return self._getIceServerObject(srvid).addChannel(name, parentid)

    def removeChannel(self, srvid, channelid):
        return self._getIceServerObject(srvid).removeChannel(channelid)

    def renameChannel(self, srvid, channelid, name, description):
        srv = self._getIceServerObject(srvid)
        state = srv.getChannelState(channelid)
        state.name = name
        srv.setChannelState(state)

    def moveChannel(self, srvid, channelid, parentid):
        srv = self._getIceServerObject(srvid)
        state = srv.getChannelState(channelid)
        state.parent = parentid
        srv.setChannelState(state)

    def moveUser(self, srvid, sessionid, channelid):
        srv = self._getIceServerObject(srvid)
        state = srv.getState(sessionid)
        state.channel = channelid
        srv.setState(state)

    def muteUser(self, srvid, sessionid, mute=True):
        srv = self._getIceServerObject(srvid)
        state = srv.getState(sessionid)
        state.mute = mute
        srv.setState(state)

    def deafenUser(self, srvid, sessionid, deaf=True):
        srv = self._getIceServerObject(srvid)
        state = srv.getState(sessionid)
        state.deaf = deaf
        srv.setState(state)

    def kickUser(self, srvid, userid, reason=""):
        return self._getIceServerObject(srvid).kickPlayer(userid, reason)

    def sendMessage(self, srvid, sessionid, message):
        return self._getIceServerObject(srvid).sendMessage(sessionid, message)

    def sendMessageChannel(self, srvid, channelid, tree, message):
        return self._getIceServerObject(srvid).sendMessageChannel(
            channelid, tree, message
        )


class MumbleCtlIce_120(MumbleCtlIce_118):
    def getRegisteredPlayers(self, srvid, filter=""):
        users = self._getIceServerObject(srvid).getRegisteredUsers(filter)
        ret = {}

        for id in users:
            ret[id] = ObjectInfo(
                userid=id, name=str(users[id], "utf8"), email="", pw=""
            )

        return ret

    def getPlayers(self, srvid):
        userdata = self._getIceServerObject(srvid).getUsers()
        for key in userdata:
            if isinstance(userdata[key], str):
                userdata[key] = userdata[key].decode("UTF-8")
        return userdata

    def getState(self, srvid, sessionid):
        userdata = self._getIceServerObject(srvid).getState(sessionid)
        for key in userdata.__dict__:
            attr = getattr(userdata, key)
            if isinstance(attr, str):
                setattr(userdata, key, attr.decode("UTF-8"))
        return userdata

    def registerPlayer(self, srvid, name, email, password):
        # To get the real values of these ENUM entries, try
        # Murmur.UserInfo.UserX.value
        import Murmur

        user = {
            Murmur.UserInfo.UserName: name,
            Murmur.UserInfo.UserEmail: email,
            Murmur.UserInfo.UserPassword: password,
        }
        return self._getIceServerObject(srvid).registerUser(user)

    def unregisterPlayer(self, srvid, mumbleid):
        self._getIceServerObject(srvid).unregisterUser(mumbleid)

    def getRegistration(self, srvid, mumbleid):
        reg = self._getIceServerObject(srvid).getRegistration(mumbleid)
        user = ObjectInfo(
            userid=mumbleid, name="", email="", comment="", hash="", pw=""
        )
        import Murmur

        if Murmur.UserInfo.UserName in reg:
            user.name = reg[Murmur.UserInfo.UserName]
        if Murmur.UserInfo.UserEmail in reg:
            user.email = reg[Murmur.UserInfo.UserEmail]
        if Murmur.UserInfo.UserComment in reg:
            user.comment = reg[Murmur.UserInfo.UserComment]
        if Murmur.UserInfo.UserHash in reg:
            user.hash = reg[Murmur.UserInfo.UserHash]
        return user

    def setRegistration(self, srvid, mumbleid, name, email, password):
        import Murmur

        user = {
            Murmur.UserInfo.UserName: name,
            Murmur.UserInfo.UserEmail: email,
            Murmur.UserInfo.UserPassword: password,
        }
        return self._getIceServerObject(srvid).updateRegistration(
            mumbleid, user
        )

    def getAllConf(self, srvid):
        conf = self.setUnicodeFlag(
            self._getIceServerObject(srvid).getAllConf()
        )

        info = {}
        for key in conf:
            if key == "playername" and conf[key]:
                # Buggy database transition from 1.1.8 -> 1.2.0
                # Store username as "username" field and set playername field to empty
                info["username"] = conf[key]
                self.setConf(srvid, "playername", "")
                self.setConf(srvid, "username", conf[key])
            else:
                info[str(key)] = conf[key]

        return info

    def getConf(self, srvid, key):
        return self._getIceServerObject(srvid).getConf(key)

    def setConf(self, srvid, key, value):
        if value is None:
            value = ""
        self._getIceServerObject(srvid).setConf(key, value)

    def getACL(self, srvid, channelid):
        return self._getIceServerObject(srvid).getACL(channelid)

    def setACL(self, srvid, channelid, acls, groups, inherit):
        return self._getIceServerObject(srvid).setACL(
            channelid, acls, groups, inherit
        )

    def getBans(self, srvid):
        return self._getIceServerObject(srvid).getBans()

    def setBans(self, srvid, bans):
        return self._getIceServerObject(srvid).setBans(bans)

    def addBanForSession(self, srvid, sessionid, **kwargs):
        session = self.getState(srvid, sessionid)
        if "bits" not in kwargs:
            kwargs["bits"] = 128
        if "start" not in kwargs:
            kwargs["start"] = int(time())
        if "duration" not in kwargs:
            kwargs["duration"] = 3600
        return self.addBan(srvid, address=session.address, **kwargs)

    def addBan(self, srvid, **kwargs):
        for key in kwargs:
            if isinstance(kwargs[key], str):
                kwargs[key] = kwargs[key]

        from Murmur import Ban

        srvbans = self.getBans(srvid)
        srvbans.append(Ban(**kwargs))
        return self.setBans(srvid, srvbans)

    def removeBan(self, srvid, **kwargs):
        return self.setBans(
            srvid,
            [
                # keep all bans which don't match exactly the one we're looking for
                ban
                for ban in self.getBans(srvid)
                # if one of those attr checks fails (-> False), min() is False -> keep the thing
                if not min([getattr(ban, kw) == kwargs[kw] for kw in kwargs])
            ],
        )

    def kickUser(self, srvid, userid, reason=""):
        return self._getIceServerObject(srvid).kickUser(userid, reason)

    def renameChannel(self, srvid, channelid, name, description):
        srv = self._getIceServerObject(srvid)
        state = srv.getChannelState(channelid)
        state.name = name
        state.description = description
        srv.setChannelState(state)

    def getUptime(self, srvid):
        return None


class MumbleCtlIce_122(MumbleCtlIce_120):
    def getUptime(self, srvid):
        return self._getIceServerObject(srvid).getUptime()


class MumbleCtlIce_123(MumbleCtlIce_120):
    def getUptime(self, srvid):
        return self._getIceServerObject(srvid).getUptime()
